#include "NewtonPendulumRenderer.h"
#include "Shaders.h"
#include <iostream>
#include "Ball.h"

using namespace std;
RendererBase* createRenderer()
{
	return new NewtonPendulumRenderer;
}

NewtonPendulumRenderer::NewtonPendulumRenderer() : ballCount_(4), maxBallCount_(9)
{
	scene_ = new Node();
	scene_->retain();
	
	Shader* ballShader = new Shader(ballShader_v, ballShader_f);
	ShaderCache::sharedShaderCache()->addShader("ballShader", ballShader);
	ballShader->setUniform(ballShader->getUniformID("u_LightPos"), 0.0f,2.0f,3.0f);

	Mesh::VertexInfo vertices[4];
	Texture2D* floorTexture = new Texture2D;
	char data[64];
	for(int i=0;i<64;i++)
		data[i] = ((i+i/8)%2) * 215+32;
	floorTexture->bufferData(GL_LUMINANCE,8,8,data);
	floorTexture->setScaleFilter(GL_NEAREST,GL_NEAREST);
	floorTexture->setWrapMode(GL_REPEAT, GL_REPEAT);	
	vertices[0].x = -static_cast<float>(100); vertices[0].y = 0; vertices[0].z = -static_cast<float>(100); vertices[0].u =  0; vertices[0].v =  0; vertices[0].nx = 0; vertices[0].ny =1; vertices[0].nz = 0;
	vertices[1].x = +static_cast<float>(100); vertices[1].y = 0; vertices[1].z = -static_cast<float>(100); vertices[1].u = 10; vertices[1].v =  0; vertices[1].nx = 0; vertices[1].ny =1; vertices[1].nz = 0;
	vertices[2].x = -static_cast<float>(100); vertices[2].y = 0; vertices[2].z = +static_cast<float>(100); vertices[2].u =  0; vertices[2].v = 10; vertices[2].nx = 0; vertices[2].ny =1; vertices[2].nz = 0;
	vertices[3].x = +static_cast<float>(100); vertices[3].y = 0; vertices[3].z = +static_cast<float>(100); vertices[3].u = 10; vertices[3].v = 10; vertices[3].nx = 0; vertices[3].ny =1; vertices[3].nz = 0;
	short indices[] = {0,2,1,1,2,3};
	floor = new Mesh(vertices,4,indices,6);
	floor->setTexture(floorTexture);
	floor->setPosition(0,-0.1,0);
	//floor->setShader(lightShader);
	scene_->addChild(floor);

	balls_ = new Ball*[ballCount_];
	for(int i=0;i<maxBallCount_;i++)
	{
		balls_[i] = new Ball(20,20);
		balls_[i]->setPosition((i*2 - ballCount_ + 1),3,0);
		scene_->addChild(balls_[i]);
	}
	for(int i=ballCount_;i<maxBallCount_;i++)
		balls_[i]->setVisible(false);
	//balls_[0]->setRotation(M_PI / 2,0,0);
	camera_ = new Camera;
	camera_->setAsActive();
	(new Node)->addChild(camera_);
	(new Node)->addChild(camera_->getParent());
	dynamic_cast<Object*>(camera_->getParent()->getParent())->retain();
	
	camera_->setPosition(0,0,5);
	camera_->getParent()->setRotation(-M_PI / 8,0,0);
	camera_->getParent()->getParent()->setRotation(0,0,0);
	camera_->getParent()->getParent()->setPosition(0,3,0);
	accumeT_ = 0;
	glEnable(GL_DEPTH_TEST);	
}

void NewtonPendulumRenderer::reshape(int w, int h)
{	
	width_ = w;
	height_ = h;	
	float aspect = static_cast<float>(w)/h;	
	camera_->setPerspective(60,aspect, 1e-2, 1e3);	
}

void NewtonPendulumRenderer::mouse(int b, int x,int y)
{

}

void NewtonPendulumRenderer::keyboard(int k)
{
	if (k == 27)
		exit(0);	
	kmVec3 up,front,right,res;
	  res.x = 0;  res.y = 0;  res.z = 0;
	   up.x = 0;   up.y = 1;   up.z = 0;
	front.x = 0;front.y = 0;front.z = 1;
	right.x = 1;right.y = 0;right.z = 0;
	kmMat4 cameraRotation = camera_->getWorldTransformationMatrix();	
	cameraRotation.mat[3] = 
		cameraRotation.mat[7] = 
		cameraRotation.mat[11] = 
		cameraRotation.mat[12] = 
		cameraRotation.mat[13] = 
		cameraRotation.mat[14] = 0;
	cameraRotation.mat[15] = 1;
	if (k == 'q' || k == 'z')
		kmVec3Transform(&res,&up,&cameraRotation);
	if (k == 'a' || k == 'd')
		kmVec3Transform(&res,&right,&cameraRotation);
	if (k == 's' || k == 'w')
		kmVec3Transform(&res,&front,&cameraRotation);
	if (k == 'z' || k == 'a' || k == 'w')
		kmVec3Scale(&res,&res, -1);
	else
		kmVec3Scale(&res,&res, 1);
	kmVec3Add(&res,&res,&camera_->getParent()->getParent()->getPosition());
	camera_->getParent()->getParent()->setPosition(res);	
	if (k == 'j')
	{
		kmVec3 p = camera_->getParent()->getParent()->getRotation();
		p.y += 0.02;
		camera_->getParent()->getParent()->setRotation(p);
	}
	if (k == 'l')
	{
		kmVec3 p = camera_->getParent()->getParent()->getRotation();
		p.y -= 0.02;
		camera_->getParent()->getParent()->setRotation(p);
	}
	if (k == 'k')
	{
		kmVec3 p = camera_->getParent()->getRotation();
		p.x += 0.02;
		camera_->getParent()->setRotation(p);
	}
	if (k == 'i')
	{
		kmVec3 p = camera_->getParent()->getRotation();
		p.x -= 0.02;
		camera_->getParent()->setRotation(p);
	}
	
	if (k >= '1' && k <= '1' + ballCount_)
	{
		balls_[k - '1']->nextMaterial();
	}
	if (k =='+' && ballCount_ < maxBallCount_)
	{
		balls_[ballCount_]->setVisible(true);
		ballCount_++;
		for(int i=0;i<maxBallCount_;i++)		
			balls_[i]->setPosition((i*2 - ballCount_ + 1),3,0);			
	}
	if (k =='-' && ballCount_ > 1)
	{		
		ballCount_--;
		balls_[ballCount_]->setVisible(false);
		for(int i=0;i<maxBallCount_;i++)		
			balls_[i]->setPosition((i*2 - ballCount_ + 1),3,0);		
	}

}

void NewtonPendulumRenderer::display(float dt)
{	
	accumeT_ += dt;	
	float x = sin(accumeT_ * 2) * 0.25;
	int target;
	if (x > 0)
		target = ballCount_ - 1;
	else
		target = 0;
	balls_[target]->setPosition((target*2 - ballCount_ + 1) + x,4 - cos(asin(x)),0);

	glViewport(0, 0, RMapSize , RMapSize);
	for(int i=0;i<3;i++)
	{
		for(int i=0;i<ballCount_;i++)
			balls_[i]->prerender(scene_);
	}
	GLGlobals::sharedGLGlobals()->setRenderTarget(nullptr);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	glViewport(0,0,width_,height_);
	camera_->setAsActive();
	scene_->preRender();		
	scene_->render();	
	
}