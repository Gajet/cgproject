#include "ExplodableMesh.h"
#include "Shaders.h"
#include <algorithm>
using namespace std;

ExplodableMesh::ExplodableMesh()
{
	glGenBuffers(2, dataBuf);
	vector<std::pair<int,const char*>> feedBackOrder;
	feedBackOrder.push_back(make_pair((int)&reinterpret_cast<movableVertexinfo*>(0)->position,"v_Position"));
	feedBackOrder.push_back(make_pair((int)&reinterpret_cast<movableVertexinfo*>(0)->velocity,"v_Velocity"));
	feedBackOrder.push_back(make_pair((int)&reinterpret_cast<movableVertexinfo*>(0)->normal,"v_Normal"));
	feedBackOrder.push_back(make_pair((int)&reinterpret_cast<movableVertexinfo*>(0)->textureCoordination,"v_TexCOORD"));
	feedBackOrder.push_back(make_pair((int)&reinterpret_cast<movableVertexinfo*>(0)->color,"v_Color"));
	feedBackOrder.push_back(make_pair((int)&reinterpret_cast<movableVertexinfo*>(0)->length,"v_Length"));
	
	sort(feedBackOrder.begin(),feedBackOrder.end());
	vector<const char*> feedBacks;
	for(unsigned i=0;i<feedBackOrder.size();i++)
		feedBacks.push_back(feedBackOrder[i].second);
	initExplosion = new TransformFeedbackShader(simulationInit_v,simulationInit_g,feedBacks);
	stepExplosion = new TransformFeedbackShader(simulationStep_v,simulationStep_g,feedBacks);
	initExplosion->retain();
	stepExplosion->retain();
	initShaderPowerLocation = initExplosion->getUniformID("u_Power");
	initShaderOriginLocation = initExplosion->getUniformID("u_Origin");
	stepShaderDTLocation = stepExplosion->getUniformID("u_DT");
	stepShaderGravityLocation = stepExplosion->getUniformID("u_Gravity");
	stepShaderVelocityLocation = stepExplosion->getAttribitueID("a_Velocity");
	stepShaderLengthLocation = stepExplosion->getAttribitueID("a_Length");
	targetFeedBackID = 0;
}

ExplodableMesh::~ExplodableMesh()
{	
	initExplosion->release();
	stepExplosion->release();
	glDeleteBuffers(2, dataBuf);
}

void ExplodableMesh::prepare()
{
	GLGlobals::sharedGLGlobals()->setArrayBuffer(getArrayBufferID());
	GLGlobals::sharedGLGlobals()->setElementArrayBuffer(getElementArrayBufferID());
	int arrayBufferSize;
	glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &arrayBufferSize);	
	char* arrayData = (char*)malloc(arrayBufferSize);
	short* elementData = new short[indicesCount_];
	movableVertexinfo* vertices = new movableVertexinfo[indicesCount_];
	glGetBufferSubData(GL_ARRAY_BUFFER,0,arrayBufferSize,arrayData);
	glGetBufferSubData(GL_ELEMENT_ARRAY_BUFFER,0,indicesCount_ * sizeof(short), elementData);
	for(unsigned i=0;i<indicesCount_;i++)
	{
		memcpy(&vertices[i].position,arrayData + elementData[i] * vertexAttributeStride_ + (int)vertexAttributeOffsets_.positionOffset_,sizeof(float) * 3);
		memcpy(&vertices[i].normal,arrayData + elementData[i] * vertexAttributeStride_ + (int)vertexAttributeOffsets_.normalOffset_,sizeof(float) * 3);		
		memcpy(&vertices[i].color,arrayData + elementData[i] * vertexAttributeStride_ + (int)vertexAttributeOffsets_.colorOffset_,sizeof(float) * 4);
		memcpy(&vertices[i].textureCoordination,arrayData + elementData[i] * vertexAttributeStride_ + (int)vertexAttributeOffsets_.texCoordOffset_,sizeof(float) * 2);		
	}
	setData(vertices,indicesCount_);
	delete []elementData;
	GLGlobals::sharedGLGlobals()->setArrayBuffer(dataBuf[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(movableVertexinfo) * indicesCount_, nullptr, GL_STATIC_DRAW);
	GLGlobals::sharedGLGlobals()->setArrayBuffer(dataBuf[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(movableVertexinfo) * indicesCount_, nullptr, GL_STATIC_DRAW);	
	stepExplosion->setUniform(stepShaderGravityLocation,0.0f,-1.0f,0.0f);	
}

void ExplodableMesh::Explode(float cx, float cy, float cz, float power)
{
	GLGlobals::sharedGLGlobals()->setArrayBuffer(getArrayBufferID());
	GLGlobals::sharedGLGlobals()->setElementArrayBuffer(getElementArrayBufferID());
	GLGlobals::sharedGLGlobals()->setTransformFeedbackBuffer(dataBuf[1 - targetFeedBackID]);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.color, true);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.normals, true);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.position, true);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.textureCoordinates, true);
	
	glVertexAttribPointer(vertexAttributeIDs_.position, 3, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.positionOffset_);
	glVertexAttribPointer(vertexAttributeIDs_.normals, 3, GL_FLOAT, true, vertexAttributeStride_, vertexAttributeOffsets_.normalOffset_);	
	glVertexAttribPointer(vertexAttributeIDs_.color, 3, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.colorOffset_);
	glVertexAttribPointer(vertexAttributeIDs_.textureCoordinates, 2, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.texCoordOffset_);	

	initExplosion->setUniform(initShaderPowerLocation, power);
	initExplosion->setUniform(initShaderOriginLocation,cx,cy,cz);

	initExplosion->compute(GL_TRIANGLES, indicesCount_, true);	
	
}

void ExplodableMesh::step(float dt)
{
	GLGlobals::sharedGLGlobals()->setArrayBuffer(dataBuf[1 - targetFeedBackID]);	
	GLGlobals::sharedGLGlobals()->setTransformFeedbackBuffer(dataBuf[targetFeedBackID]);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.color, true);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.normals, true);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.position, true);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.textureCoordinates, true);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(stepShaderVelocityLocation, true);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(stepShaderLengthLocation, true);
	
	glVertexAttribPointer(vertexAttributeIDs_.position, 3, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.positionOffset_);
	glVertexAttribPointer(vertexAttributeIDs_.normals, 3, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.normalOffset_);
	glVertexAttribPointer(vertexAttributeIDs_.color, 3, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.colorOffset_);
	glVertexAttribPointer(vertexAttributeIDs_.textureCoordinates, 2, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.texCoordOffset_);
	glVertexAttribPointer(stepShaderVelocityLocation, 3, GL_FLOAT, false, sizeof(movableVertexinfo), &(reinterpret_cast<movableVertexinfo*>(0)->velocity));
	glVertexAttribPointer(stepShaderLengthLocation, 1, GL_FLOAT, false, sizeof(movableVertexinfo), &(reinterpret_cast<movableVertexinfo*>(0)->length));
	
	stepExplosion->setUniform(stepShaderDTLocation, dt);
	stepExplosion->compute(GL_TRIANGLES, indicesCount_,false);
	targetFeedBackID = 1 - targetFeedBackID;
}

void ExplodableMesh::render()
{
	int temp = VBOID_->vertices;
	VBOID_->vertices = dataBuf[targetFeedBackID];
	Mesh::render();
	VBOID_->vertices = temp;
}