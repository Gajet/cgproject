#include "Shaders.h"
#define STRINGIFY(X) #X

const char* simulationInit_v = STRINGIFY(
	#version 150									\n
													\n
	in vec3 a_Position;								\n	
	in vec3 a_Normal;								\n
	in vec2 a_TexCOORD;								\n
	in vec4 a_Color;								\n
	out vec2 g_TexCOORD;							\n
	out vec3 g_Normal;								\n
	out vec3 g_Position;							\n
	out vec4 g_Color;								\n
													\n
	void main()										\n
	{												\n
		g_Color = a_Color;							\n
		g_Normal = a_Normal;						\n
		g_TexCOORD = a_TexCOORD;					\n
		g_Position = a_Position;					\n
	}
	);

const char* simulationInit_g = STRINGIFY(
	#version 150										\n
														\n
	layout(triangles) in;								\n
	layout(triangle_strip, max_vertices = 3) out;		\n
	uniform float u_Power;								\n
	uniform vec3 u_Origin;								\n
														\n
	in vec3 g_Position[];								\n
	in vec2 g_TexCOORD[];								\n
	in vec3 g_Normal[];									\n
	in vec4 g_Color[];									\n
	out vec2 v_TexCOORD;								\n
	out vec3 v_Normal;									\n
	out vec3 v_Position;								\n
	out vec4 v_Color;									\n
	out vec3 v_Velocity;								\n
	out float v_Length;									\n
														\n
	void main()											\n
	{													\n
		int i;											\n
		float len;
				
		v_Normal = cross(g_Position[0] - g_Position[1],
			g_Position[0] - g_Position[2]);
		for (i=0;i<3;i++)								\n
		{												\n
			v_Color = g_Color[i];						\n			
			v_TexCOORD = g_TexCOORD[i];					\n
			v_Position = g_Position[i]*0.5 + vec3(0,7,0);	\n			
			v_Length = length (g_Position[i] -			
				g_Position[(i+1)%3]) / 2.0;					\n
			vec3 dist = (v_Position- u_Origin);		\n
			len = length(dist);							\n
			v_Velocity = dist / len  * u_Power / len;	\n
			EmitVertex();								\n
		}												\n
		EndPrimitive();									\n
	}									
	);

const char* simulationStep_v = STRINGIFY(
	#version 150									\n
													\n
	in vec3 a_Position;								\n	
	in vec3 a_Normal;								\n
	in vec2 a_TexCOORD;								\n
	in vec4 a_Color;								\n
	in vec3 a_Velocity;								\n
	in float a_Length;								\n
	out vec2 g_TexCOORD;							\n
	out vec3 g_Normal;								\n
	out vec3 g_Position;							\n
	out vec4 g_Color;								\n
	out vec3 g_Velocity;							\n
	out float g_Length;								\n
													\n
	void main()										\n
	{												\n
		g_Color = a_Color;							\n
		g_Normal = a_Normal;						\n
		g_TexCOORD = a_TexCOORD;					\n
		g_Position = a_Position;					\n
		g_Velocity = a_Velocity;					\n
		g_Length = a_Length;						\n
	}
	);


const char* simulationStep_g = STRINGIFY(
	#version 150									\n
													\n
	layout(triangles) in;							\n
	layout(triangle_strip, max_vertices = 3) out;	\n
	uniform float u_DT;								\n
	uniform vec3 u_Gravity;							\n
	in vec3 g_Position[3];							\n	
	in vec3 g_Normal[3];							\n
	in vec2 g_TexCOORD[3];							\n
	in vec4 g_Color[3];								\n
	in vec3 g_Velocity[3];							\n
	in float g_Length[3];							\n
	out vec2 v_TexCOORD;							\n
	out vec3 v_Normal;								\n
	out vec3 v_Position;							\n
	out vec4 v_Color;								\n
	out vec3 v_Velocity;							\n
	out float v_Length;								\n	
													\n
	void main()										\n
	{												\n
		vec3 tp[3];									\n
		vec3 tv[3];									\n
		vec3 f[3];									\n
													\n
		float step = 0.02;							\n
		float dt = step * u_DT;						\n
		float s;									\n
		float l;
		vec3 d;										\n
		int i;										\n
		for(i=0;i<3;i++)							\n
		{											\n
			tp[i] = g_Position[i];					\n
			tv[i] = g_Velocity[i];					\n			
		}											\n
		for(s=0;s<1.0;s+=step)						\n		
			for(i=0;i<3;i++)						\n
			{										\n
				f[i] = u_Gravity;					\n
				d = tp[i] - tp[(i+1)%3];			\n
				l = length(d);						\n
				f[i] += d /l *(g_Length[i]-l) * 1000;		\n 
				d = tp[i] - tp[(i+2)%3];			\n
				l = length(d);						\n
				f[i] += d /l *(g_Length[(i+2)%3]-l) * 1000;\n 				
				tv[i] = tv[i] * pow(0.8,dt);		\n
				tv[i] += f[i] * dt;					\n
				tp[i] += tv[i] * dt;				\n
				if(tp[i].y < 0)						\n
				{									\n
					tp[i].y = -tp[i].y;				\n
					tv[i].y = -tv[i].y;				\n
				}									\n
			}										\n
		v_Normal = cross(tp[0] - tp[1], tp[0] - tp[2]);
		for(i=0;i<3;i++)							\n
		{											\n
			v_Color = g_Color[i];					\n
			v_TexCOORD = g_TexCOORD[i];				\n
			v_Length = g_Length[i];					\n		
			v_Velocity = tv[i];						\n
			v_Position = tp[i];						\n
			EmitVertex();							\n
		}											\n
		EndPrimitive();								\n
	}
	);

