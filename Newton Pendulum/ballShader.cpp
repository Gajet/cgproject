#include "Shaders.h"
#define STRINGIFY(X) #X

const char* ballShader_v = STRINGIFY(
	#version 150									\n
													\n
	uniform mat4 u_Pmat;							\n
	uniform mat4 u_Vmat;							\n
	uniform mat4 u_Mmat;							\n
	in vec4 a_Position;								\n	
	in vec2 a_TexCOORD;								\n
	out vec2 v_TexCOORD;							\n
	out vec3 v_normal;								\n
	out vec4 v_pos;									\n
	out vec4 pos;
	out vec3 ray;
													\n
	void main()										\n
	{												\n
		gl_Position = pos = u_Pmat * u_Vmat * u_Mmat * a_Position;		\n		
		
		v_pos = u_Mmat * a_Position;				\n
		mat3 nMat = mat3(u_Mmat);					\n
		nMat = transpose(inverse(nMat));			\n
		v_normal = nMat * a_Position.xyz;			\n
		v_TexCOORD = a_TexCOORD;

		ray = (u_Mmat * a_Position - inverse(u_Vmat) * vec4(0,0,0,1)).xyz;
	}
	);

const char* ballShader_f = STRINGIFY(
	#version 130									\n
													\n	
	uniform vec4 u_Color;							\n	
	uniform vec3 u_LightPos;						\n
	uniform samplerCube u_Texture;					\n
	uniform int material;
	in vec3 v_normal;								\n
	in vec4 v_pos;									\n
	in vec2 v_TexCOORD;								\n	
	in vec4 pos;
	in vec3 ray;
	out vec4 fColor;								\n
													\n
	void main()										\n
	{												\n
		vec3 light2frag = normalize(u_LightPos - v_pos.xyz);
		vec3 normal = normalize(v_normal);
		vec3 cam2frag = normalize(ray);
		vec3 specDir = reflect(light2frag,normal);
		vec3 ambient = vec3(0.1);						\n
		vec3 diffuse = 0.9 * vec3(max(0.0,dot(normal,light2frag)));	\n
		vec3 specular = vec3(pow(max(dot(cam2frag, specDir), 0.0), 50));
		vec3 ref = reflect(normalize(ray),v_normal);		
		vec3 mirror = texture(u_Texture,ref).xyz;
		vec3 glass = texture(u_Texture,normalize((-ref * 2.0 + normalize(ray))/3.0)).xyz;
		if (material == 2)
			fColor.xyz = (ambient + diffuse) * (glass + mirror / 30) + specular / 2;
		if (material == 1)
			fColor.xyz = (ambient + diffuse) * (mirror) + specular;
		if (material == 0)
			fColor.xyz = (ambient + diffuse) * (0.5 + mirror / 20) + specular / 4;
		fColor.a = u_Color.a;
	}
	);