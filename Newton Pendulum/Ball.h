#pragma once
#include <Engine.h>
#define RMapSize 256

class Ball : public Node
{
public:
	enum Material
	{
		Metal=0, 
		Glass=2,
		Mirror=1
	};
	Ball();
	Ball(unsigned vSlice, unsigned hSlice);
	~Ball();
	void reshape(unsigned vSlice, unsigned hSlice);
	void nextMaterial();
	void render();
	void prerender(Node* scene);	
protected:
	void setUpCameras();
	void setUpTextures();
	Material material_;
	Mesh* sphere_;
	Camera* cameras_[6];
	TextureCube* ballTexture_;
	RenderTexture* renderTarget_;
	unsigned materialLocation_;
};