#pragma once
#include "Engine.h"

class ExplodableMesh : public Mesh
{
public:
	ExplodableMesh();
	~ExplodableMesh();
	void prepare();
	void Explode(float cx, float cy, float cz, float power);
	void step(float dt);
	void render();
protected:
	struct movableVertexinfo : public Mesh::VertexInfo{
		union{
			struct{
			float vx,vy,vz;
			};
			struct{
				float x,y,z;
			} velocity;
		};
		float length;
	};
	struct {
		GLuint dataBuf[2];
	};
	struct {
		int initShaderOriginLocation;
		int initShaderPowerLocation;
		int stepShaderDTLocation;
		int stepShaderGravityLocation;
		int stepShaderVelocityLocation;
		int stepShaderLengthLocation;
	};
	int targetFeedBackID;
	TransformFeedbackShader *initExplosion, *stepExplosion;
};