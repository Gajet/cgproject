#include <Shared.h>
#include <Engine.h>
#include "ExplodableMesh.h"

struct ExplosionRenderer : public RendererBase{
public:
	ExplosionRenderer();
	virtual void display(float dt);
	virtual void reshape(int w,int h);
	virtual void keyboard(int k);
	virtual void mouse(int bs, int x,int y);
protected:
	Node* scene_;
	Camera* camera_;	
	ExplodableMesh* explosionObject_;
	TransformFeedbackShader* feedbackShader_;
	bool simulationRunning_;
	bool exploded_;
	int width_,height_;
};

RendererBase* createRenderer();
