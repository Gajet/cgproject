#include "SolarSystemRenderer.h"
#include <fstream>
#include <string>
#include <iostream>
#include <thread>
#define DETAIL 20

using namespace std;
using namespace std::chrono;

RendererBase* createRenderer()
{
	return new SolarSystemRenderer;
}

SolarSystemRenderer::SolarSystemRenderer()
{
	time = 0;
	Mesh::VertexInfo vertices[(DETAIL * 2) * (DETAIL - 1) * 2 * 3 + (DETAIL * 2) * 2 * 3];
	int vid;
	vertices[0].x = 0;
	vertices[0].y = 0;
	vertices[0].z = 0;			
	vid = 0;
	for(float i=1;i<DETAIL;i++)
		for(float j=0;j<DETAIL * 2;j++)
		{
			vertices[vid].y = sin((i + 0.5f) * M_PI / DETAIL);
			vertices[vid].x = vertices[vid].y * cos(j * M_PI / DETAIL);
			vertices[vid].z = vertices[vid].y * sin(j * M_PI / DETAIL);
			vertices[vid].y = cos((i + 0.5f) * M_PI / DETAIL);
			vertices[vid].u = j / DETAIL / 2;
			vertices[vid].v = (i+0.5) / DETAIL;
			vid ++;	
			vertices[vid].y = sin((i - 0.5f) * M_PI / DETAIL);
			vertices[vid].x = vertices[vid].y * cos((j+1) * M_PI / DETAIL);
			vertices[vid].z = vertices[vid].y * sin((j+1) * M_PI / DETAIL);
			vertices[vid].y = cos((i - 0.5f) * M_PI / DETAIL);
			vertices[vid].u = (j+1) / DETAIL / 2;
			vertices[vid].v = (i-0.5) / DETAIL;
			vid ++;	
			vertices[vid].y = sin((i + 0.5f) * M_PI / DETAIL);
			vertices[vid].x = vertices[vid].y * cos((j + 1) * M_PI / DETAIL);
			vertices[vid].z = vertices[vid].y * sin((j + 1) * M_PI / DETAIL);
			vertices[vid].y = cos((i + 0.5f) * M_PI / DETAIL);
			vertices[vid].u = (j+1) / DETAIL / 2;
			vertices[vid].v = (i+0.5) / DETAIL;
			vid ++;	

			vertices[vid].y = sin((i + 0.5f) * M_PI / DETAIL);
			vertices[vid].x = vertices[vid].y * cos((j+0) * M_PI / DETAIL);
			vertices[vid].z = vertices[vid].y * sin((j+0) * M_PI / DETAIL);
			vertices[vid].y = cos((i + 0.5f) * M_PI / DETAIL);
			vertices[vid].u = j / DETAIL / 2;
			vertices[vid].v = (i+0.5) / DETAIL;
			vid ++;	
			vertices[vid].y = sin((i - 0.5f) * M_PI / DETAIL);
			vertices[vid].x = vertices[vid].y * cos(j * M_PI / DETAIL);
			vertices[vid].z = vertices[vid].y * sin(j * M_PI / DETAIL);
			vertices[vid].y = cos((i - 0.5f) * M_PI / DETAIL);
			vertices[vid].u = j / DETAIL / 2;
			vertices[vid].v = (i-0.5) / DETAIL;
			vid ++;	
			vertices[vid].y = sin((i - 0.5f) * M_PI / DETAIL);
			vertices[vid].x = vertices[vid].y * cos((j + 1) * M_PI / DETAIL);
			vertices[vid].z = vertices[vid].y * sin((j + 1) * M_PI / DETAIL);
			vertices[vid].y = cos((i - 0.5f) * M_PI / DETAIL);
			vertices[vid].u = (j+1) / DETAIL / 2;
			vertices[vid].v = (i-0.5) / DETAIL;
			vid ++;	
		}
		
	for(unsigned j=0;j<DETAIL * 2;j++)
	{		
		vertices[vid].y = sin((DETAIL - 0.5f) * M_PI / DETAIL);
		vertices[vid].x = vertices[vid].y * cos(j * M_PI / DETAIL);
		vertices[vid].z = vertices[vid].y * sin(j * M_PI / DETAIL);
		vertices[vid].y = cos((DETAIL - 0.5f) * M_PI / DETAIL);
		vertices[vid].u = j / DETAIL / 2;
		vertices[vid].v = (DETAIL-0.5) / DETAIL;
		vid ++;			
		vertices[vid].y = sin((DETAIL - 0.5f) * M_PI / DETAIL);
		vertices[vid].x = vertices[vid].y * cos((j + 1) * M_PI / DETAIL);
		vertices[vid].z = vertices[vid].y * sin((j + 1) * M_PI / DETAIL);
		vertices[vid].y = cos((DETAIL - 0.5f) * M_PI / DETAIL);
		vertices[vid].u = (j+1) / DETAIL / 2;
		vertices[vid].v = (DETAIL-0.5) / DETAIL;
		vid ++;	
		vertices[vid].x = 0;
		vertices[vid].z = 0;
		vertices[vid].y = -1;
		vertices[vid].u = 0.5;
		vertices[vid].v = 1;
		vid ++;	
		
	}

	for(unsigned j=0;j<DETAIL * 2;j++)
	{		
		vertices[vid].y = sin((0.5f) * M_PI / DETAIL);
		vertices[vid].x = vertices[vid].y * cos(j * M_PI / DETAIL);
		vertices[vid].z = vertices[vid].y * sin(j * M_PI / DETAIL);
		vertices[vid].y = cos((0.5f) * M_PI / DETAIL);
		vertices[vid].u = j / DETAIL / 2;
		vertices[vid].v = (0.5) / DETAIL;
		vid ++;			
		vertices[vid].x = 0;
		vertices[vid].z = 0;
		vertices[vid].y = 1;
		vertices[vid].u = 0.5;
		vertices[vid].v = 0;
		vid ++;	
		vertices[vid].y = sin((0.5f) * M_PI / DETAIL);
		vertices[vid].x = vertices[vid].y * cos((j + 1) * M_PI / DETAIL);
		vertices[vid].z = vertices[vid].y * sin((j + 1) * M_PI / DETAIL);
		vertices[vid].y = cos((0.5f) * M_PI / DETAIL);
		vertices[vid].u = (j+1) / DETAIL / 2;
		vertices[vid].v = (0.5) / DETAIL;
		vid ++;	
				
	}
	Shader* solarShader = new Shader(solarShader_v, solarShader_f);
	Mesh* planetTemplate = new Mesh(vertices, vid);
	planetTemplate->setAutoSelectShader(false);
	planetTemplate->setShader(solarShader);		
	planetTemplate->setTexture(new Texture2D("night.jpg"));
	
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	
	
	campos.x = campos.y = 0;
	campos.z = 240;
	
	system_ = new Node;
	system_->retain();
	
	ifstream list("system.conf");
	string s;
	getline(list,s);
	float id,parent,dist,omega,spin,rad,r,g,b;
	while(list>>id)
	{
		list >> parent>>dist>>omega>>spin>>rad>>r>>g>>b;
		planetData data;
		data.node = new Node;
		(new Node)->addChild(data.node);		
		Mesh* planet = new Mesh(*planetTemplate);		
		data.node->addChild(planet);
		string name;
		getline(list, name);
		data.node->setPosition(dist,0,0);
		planet->setTexture(new Texture2D(("images/" + name.substr(1)+".jpg").c_str()));		
		planet->setScale(rad,rad,rad);
		if (parent != -1)
		{
			planets_[parent].node->addChild(data.node->getParent());
			planet->setUniformColor(1,1,1, (255 - id) / 256.f);
		}
		else
		{
			system_->addChild(data.node->getParent());
			planet->setUniformColor(20,20,20, (255 - id) / 256.f);			
		}
		cout << id << ":" <<(255 - id) / 256.f << "\n";
		data.rot_off = 0;
		data.rot_speed = omega * 10;
		data.spin = spin * 10;
		planets_.push_back(data);
	}
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ZERO);
	defaultCamera_ = new Camera;
	defaultCamera_->setAsActive();
	(new Node)->addChild(defaultCamera_);
	(new Node)->addChild(defaultCamera_->getParent());
	dynamic_cast<Object*>(defaultCamera_->getParent()->getParent())->retain();
	//planets_[3].node->addChild(defaultCamera_->getParent()->getParent());

	defaultCamera_->setPosition(0,0,campos.z);
	defaultCamera_->getParent()->setRotation(campos.x,0,0);
	defaultCamera_->getParent()->getParent()->setRotation(0,campos.y,0);

	Texture2D* galaxy = new Texture2D("images/b1.jpg");
	vertices[0].x = -static_cast<float>(galaxy->getWidth()); vertices[0].y = -static_cast<float>(galaxy->getHeight());vertices[0].z = -1; vertices[0].u = 0; vertices[0].v = 0;
	vertices[1].x = +static_cast<float>(galaxy->getWidth()); vertices[1].y = -static_cast<float>(galaxy->getHeight());vertices[1].z = -1; vertices[1].u = 1; vertices[1].v = 0;
	vertices[2].x = -static_cast<float>(galaxy->getWidth()); vertices[2].y = +static_cast<float>(galaxy->getHeight());vertices[2].z = -1; vertices[2].u = 0; vertices[2].v = 1;
	vertices[3].x = +static_cast<float>(galaxy->getWidth()); vertices[3].y = +static_cast<float>(galaxy->getHeight());vertices[3].z = -1; vertices[3].u = 1; vertices[3].v = 1;
	short indices[] = {0,1,2,1,3,2};
	sky_= new Mesh(vertices,4,indices,6);
	sky_->setTexture(galaxy);	
	orthoCamera_ = new Camera;
}

void SolarSystemRenderer::reshape(int w, int h)
{		
	glViewport(0,0,w,h);		
	float aspect = static_cast<float>(w)/h;	
	defaultCamera_->setPerspective(60,aspect, 1e-3, 1e3);	
	orthoCamera_->setOrthogonal(aspect, 1e-3,1e3);
	float backScaleX = aspect /  static_cast<float>(static_cast<Texture2D*>(sky_->getTexture())->getWidth());
	float backScaleY = 1 / static_cast<float>(static_cast<Texture2D*>(sky_->getTexture())->getHeight());
	float skyScale = max(backScaleX,backScaleY);
	sky_->setScale(skyScale,skyScale,skyScale);
	windowHeight_ = h;
}

void SolarSystemRenderer::mouse(int bs,int x,int y)
{
	static int bd[] = {false,false,false};
	bd[bs] = !bd[bs];
	y =windowHeight_ - y;
	if(bd[0])
	{
		float buf[4];
		glReadPixels(x,y, 1,1,GL_RGBA, GL_FLOAT, buf);
		int id = 255 - buf[3] * 256;		
		{
			Node* defaultCameraRoot = dynamic_cast<Node*>(defaultCamera_->getParent()->getParent());
			Node* oldParent = dynamic_cast<Node*>(defaultCameraRoot->getParent());
			if (oldParent)
			{
				auto i = oldParent->childrenBegin();
				while(i!=oldParent->childrenEnd())
				{
					if (*i == defaultCameraRoot)
						break;				
					i++;
				}
				oldParent->removeChild(i);
			}
			if (id >=0)
				planets_[id].node->addChild(defaultCameraRoot);
		}
	}
}

void SolarSystemRenderer::keyboard(int k)
{
	if (k == 27)
		exit(0);
	if (k == 'q' && campos.z > 100)
		campos.z *= 0.9f;
	if (k == 'z' && campos.z < 500)
		campos.z /= 0.9f;
	if (k == 'a')
		campos.y -=0.05;
	if (k == 'd')
		campos.y +=0.05;
	if (k == 's' &&  campos.x >= M_PI / -2)
		campos.x -=0.05;
	if (k == 'w' && campos.x <= M_PI / 2)
		campos.x +=0.05;
	defaultCamera_->setPosition(0,0,campos.z);
	defaultCamera_->getParent()->setRotation(campos.x,0,0);
	defaultCamera_->getParent()->getParent()->setRotation(0,campos.y,0);
	//orthoCamera_->setVeiw(eye,target,up);
}

void SolarSystemRenderer::display(float dt)
{	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	time += dt;
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	orthoCamera_->setAsActive();
	sky_->preRender();
	sky_->render();
	glFlush();
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	defaultCamera_->setAsActive();
	for(unsigned i=0;i<planets_.size();i++)		
	{
		planets_[i].node->getParent()->setRotation(0,(time * planets_[i].rot_speed + planets_[i].rot_off) * M_PI / 180,0);
		planets_[i].node->getChildren()[0]->setRotation(0,(time * planets_[i].spin) * M_PI / 180,0);
	}
	system_->preRender();
	system_->render();
	orthoCamera_->setAsActive();	
	glFlush();
}

#define STRINGIFY(X) #X

const char* solarShader_v = STRINGIFY(
	#version 150									\n
													\n
	uniform mat4 u_Vmat;							\n
	uniform mat4 u_Pmat;							\n
	uniform mat4 u_Mmat;							\n
	in vec4 a_Position;							\n	
	in vec2 a_TexCOORD;								\n
	out vec2 v_TexCOORD;							\n
	out vec3 v_normal;								\n
	out vec4 v_pos;									\n
													\n
	void main()										\n
	{												\n
		gl_Position = u_Pmat * u_Vmat * u_Mmat * a_Position;		\n		
		v_pos = u_Mmat * a_Position;				\n
		mat3 nMat = mat3(u_Mmat);					\n
		nMat = transpose(inverse(nMat));			\n
		v_normal = nMat * a_Position.xyz;			\n
		v_TexCOORD = a_TexCOORD;
	}
	);

const char* solarShader_f = STRINGIFY(
	#version 140									\n
													\n	
	uniform vec4 u_Color;							\n
	uniform sampler2D u_Texture;					\n
	in vec3 v_normal;								\n
	in vec4 v_pos;									\n
	in vec2 v_TexCOORD;								\n
	out vec4 fColor;
													\n
	void main()										\n
	{												\n
		vec4 light = vec4(0.1);						\n
		light += 0.9 * max(dot(normalize(v_normal),normalize(-v_pos.xyz)), 0);
		fColor.xyz = u_Color.xyz * light.xyz * texture2D(u_Texture,v_TexCOORD).xyz;						\n
		fColor.a = u_Color.a;
	}
	);