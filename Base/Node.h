#pragma once
#include "Object.h"
#include "RenderableProtocol.h"
#include <vector>

class Node : public Object, public RenderableProtocol
{
public:
	Node();
	virtual ~Node();
	
	void preRender();
	void render();
	void postRender();

	typedef PositionProtocol* ChildType;
	typedef std::vector<ChildType> ChildArrayType;
	typedef ChildArrayType::iterator ChildIterator;
	typedef ChildArrayType::const_iterator ConstChildIterator;
	
	void addChild(ChildType child);
	void removeChild(ChildIterator child);
	ChildIterator childrenBegin();
	ChildIterator childrenEnd();
	const ChildArrayType& getChildren();
	ConstChildIterator childrenBegin() const;
	ConstChildIterator childrenEnd() const;

private:	
	ChildArrayType children_;	
};

