#pragma once
#include "Object.h"
#include <gl/GL.h>
#include "Texture.h"

class Texture2D : public Texture
{
public:
	Texture2D(const char* fileName);
	Texture2D();
	~Texture2D();
	bool load(const char* fileName);
	void setWrapMode(GLenum wrapModeS, GLenum wrapModeT);
	void setScaleFilter(GLenum minifyFilter, GLenum magnifyFilter);
	unsigned getWidth();
	unsigned getHeight();	
	bool bufferData(int mode, unsigned width, unsigned height, void* data);
	GLenum getType();
private:	
	unsigned width_,height_;
	GLenum minifyFilter_, magnifyFilter_, wrapModeS_, wrapModeT_;
};