#include "pcHeaders.h"
#include "RenderTexture.h"
#include "GLGlobals.h"
RenderTexture::RenderTexture() : depthTexture_(nullptr), stencilTexture_(nullptr)
{
	glGenFramebuffers(1, &frameBufferID_);
}

RenderTexture::RenderTexture(unsigned width, unsigned height, int colorAttachments, bool depthAttachment, bool stencilAttachment) : depthTexture_(nullptr), stencilTexture_(nullptr)
{
	glGenFramebuffers(1, &frameBufferID_);
	init(width,height,colorAttachments,depthAttachment,stencilAttachment);
}

RenderTexture::~RenderTexture()
{
	glDeleteFramebuffers(1, &frameBufferID_);
}
bool RenderTexture::init(unsigned width, unsigned height, int colorAttachments, bool depthAttachment, bool stencilAttachment)
{
	if((colorAttachments > 0 || depthAttachment || stencilAttachment) == false)
		return false;
	Object* prevRenderTarget = GLGlobals::sharedGLGlobals()->getRenderTarget();
	GLGlobals::sharedGLGlobals()->setRenderTarget(this);
	unsigned oldTextureCount = colorTextures_.size();
	bool oldDepthAttachment = depthTexture_;
	bool oldStencilAttachment = stencilTexture_;
	if(colorTextures_.size() > colorAttachments)
	{
		for(unsigned i=colorAttachments;i<colorTextures_.size();i++)
		{
			colorTextures_[i]->release();
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, 0,0);
		}
		colorTextures_.resize(colorAttachments);
	}
	if(colorTextures_.size() < colorAttachments)
	{
		colorTextures_.resize(colorAttachments,nullptr);
		for(unsigned i=colorAttachments - 1;i < colorTextures_.size();i--)
			if(colorTextures_[i])
				break;
			else
			{
				colorTextures_[i] = new Texture2D;
				colorTextures_[i]->retain();			
			}
	}
	
	if(depthAttachment && !depthTexture_)
	{
		depthTexture_ = new Texture2D;
		depthTexture_->retain();		
	}
	
	if(!depthAttachment && depthTexture_)
	{
		depthTexture_->release();
		delete depthTexture_;
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0,0);
	}
	
	if(stencilAttachment && !stencilTexture_)
	{
		stencilTexture_ = new Texture2D;
		stencilTexture_->retain();		
	}
	
	if(!stencilAttachment && stencilTexture_)
	{
		stencilTexture_->release();
		delete stencilTexture_;
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, 0,0);
	}

	for(unsigned i=0;i<colorTextures_.size();i++)
		colorTextures_[i]->bufferData(GL_RGBA, width, height, nullptr);		
	if(depthTexture_)
		depthTexture_->bufferData(GL_DEPTH_COMPONENT, width, height, nullptr);
	if(stencilTexture_)
		stencilTexture_->bufferData(GL_LUMINANCE, width, height, nullptr);	

	for(unsigned i=oldTextureCount;i<colorTextures_.size();i++)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, colorTextures_[i]->getTextureID(), 0);	
	if (!oldDepthAttachment && depthAttachment)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture_->getTextureID(), 0);	
	if (!oldStencilAttachment && stencilAttachment)
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, stencilTexture_->getTextureID(), 0);	
	int completionStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);	
	GLGlobals::sharedGLGlobals()->setRenderTarget(prevRenderTarget);	
}
	
Texture2D* RenderTexture::getColorTexture(unsigned id)
{
	return id < colorTextures_.size()?colorTextures_[id]:nullptr;
}

Texture2D* RenderTexture::getDepthTexture()
{
	return depthTexture_;
}

Texture2D* RenderTexture::getStencilTexture()
{
	return stencilTexture_;
}

unsigned RenderTexture::getFrameBufferID()
{
	return frameBufferID_;
}