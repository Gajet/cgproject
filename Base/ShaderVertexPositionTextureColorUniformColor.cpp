#include "pcHeaders.h"
#include "DefaultShaders.h"

#define STRINGIFY(X) #X

const char* ShaderPositionitionTextureColorUniformColor_v = STRINGIFY(
	#version 120									\n
													\n
	uniform mat4 u_Vmat;							\n
	uniform mat4 u_Pmat;							\n
	uniform mat4 u_Mmat;							\n
	attribute vec4 a_Position;						\n
	attribute vec2 a_TexCOORD;						\n
	attribute vec4 a_Color;							\n
	varying vec4 v_Color;							\n
	varying vec2 v_TexCOORD;						\n
													\n
	void main()										\n
	{												\n
		gl_Position = u_Pmat * u_Vmat * u_Mmat * a_Position;		\n
		v_TexCOORD = a_TexCOORD;					\n
		v_Color = a_Color;							\n
	}
	);

const char* ShaderPositionitionTextureColorUniformColor_f = STRINGIFY(
	#version 120									\n
													\n	
	uniform sampler2D u_Texture;					\n
	uniform vec4 u_Color;							\n
	varying vec4 v_Color;							\n
	varying vec2 v_TexCOORD;						\n	
													\n
	void main()										\n
	{												\n
		gl_FragColor = u_Color * v_Color *		 	\n
			texture2D(u_Texture, v_TexCOORD);		\n
	}
	);