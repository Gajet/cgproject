#pragma once
#include "Texture.h"
#include <gl/GL.h>

class TextureCube: public Texture
{
public:	
	TextureCube();
	~TextureCube();	
	void setWrapMode(GLenum wrapModeS, GLenum wrapModeT, GLenum wrapModeR);
	void setScaleFilter(GLenum minifyFilter, GLenum magnifyFilter);
	GLenum getType();
protected:
	GLenum minifyFilter_, magnifyFilter_, wrapModeS_, wrapModeT_, wrapModeR_;
};