#pragma once
#include <map>
#include <string>
#include "Texture2D.h"

class TextureCache
{
public:
	TextureCache();
	~TextureCache();
	static TextureCache* sharedTextureCache();
	Texture2D* addTexture(const char* path);
	void removeTexture(const char* path);
	void removeTexture(Texture2D* texture);
	
private:
	static TextureCache* sharedTextureCache_;
	std::map<std::string, Texture2D*> cachedTextures_;
};