#include "pcHeaders.h"
#include "GLGlobals.h"
#include "ShaderBase.h"
#include "RenderTexture.h"
#include "Texture.h"

GLGlobals* GLGlobals::sharedGlobals_ = nullptr;

GLGlobals::GLGlobals()
{
	arrayBuffer_ = 0;
	elementArrayBuffer_ = 0;
	transformFeedbackBuffer_ = 0;
	vertexAttributeEnabledStatus_ = 0;
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &textureUnitCount_);
	textureIDCache_ = new Texture*[textureUnitCount_];
	for(int i=0;i<textureUnitCount_;i++)
		textureIDCache_[i] = nullptr;
	activeTextureUnit = 0;
	inUseShader_ = nullptr;
	renderTarget_ = nullptr;
}

GLGlobals::~GLGlobals()
{
	delete []textureIDCache_;
}

GLGlobals* GLGlobals::sharedGLGlobals()
{
	if (sharedGlobals_ == nullptr)
		sharedGlobals_ = new GLGlobals;
	return sharedGlobals_;
}

void GLGlobals::setArrayBuffer(int buffer)
{
	if (buffer != arrayBuffer_)
	{
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		arrayBuffer_ = buffer;
	}
}

void GLGlobals::setElementArrayBuffer(int buffer)
{
	if (buffer != elementArrayBuffer_)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
		elementArrayBuffer_ = buffer;
	}
}

void GLGlobals::setTransformFeedbackBuffer(int buffer)
{
	if (buffer != transformFeedbackBuffer_)
	{
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, buffer);
		transformFeedbackBuffer_ = buffer;
	}
}

void GLGlobals::setVertexAttributeEnabled(int id, bool status)
{
	if(id < 0)
		return;
	long long newStatus = status? 1<<id :0;
	if ((vertexAttributeEnabledStatus_ & (1LL<<id)) != newStatus)
	{
		if (status)
			glEnableVertexAttribArray(id);
		else
			glDisableVertexAttribArray(id);
		vertexAttributeEnabledStatus_ ^= (1LL<<id);
	}
}


void GLGlobals::bindTexture(Texture* texture,int textureUnit)
{
	if (textureUnitCount_ < 0 || textureUnitCount_ > textureUnitCount_)
		return;
	if (textureIDCache_[textureUnit] != texture)
	{
		textureIDCache_[textureUnit] = texture;
		if (textureUnit != activeTextureUnit)
		{
			activeTextureUnit = textureUnit;
			glActiveTexture(GL_TEXTURE0 + textureUnit);
		}		
		glBindTexture(texture->getType(), texture->getTextureID());
	}
}

void GLGlobals::setRenderTarget(Object* renderTarget)
{
	RenderTexture *target = dynamic_cast<RenderTexture*> (renderTarget);;
	if(target != renderTarget_)
	{		
		if(target)
			glBindFramebuffer(GL_FRAMEBUFFER, target->getFrameBufferID());
		else
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		renderTarget_ = target;
	}
}

Object* GLGlobals::getRenderTarget()
{
	return renderTarget_;
}