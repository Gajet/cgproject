#pragma once

#define GLUT_BUILDING_LIB
#include <glew/glew.h>
#include <glut/glut.h>
#include <gl/GL.h>
#include <gl/GLU.h>


#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

struct RendererBase {
public:
	virtual void display(float dt) = 0;
	virtual void reshape(int w,int h) = 0;
	virtual void keyboard(int k) = 0;
	virtual void mouse(int bs, int x,int y) = 0;
};

RendererBase* createRenderer();
