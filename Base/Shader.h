#pragma once
#include "ShaderBase.h"

class Shader : public ShaderBase
{	
public:
	Shader(const char* vertexShader, const char* fragmentShader);
	Shader();
	~Shader();
	bool compile(const char* vertexShader, const char* fragmentShader);
};