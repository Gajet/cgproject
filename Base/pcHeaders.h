#include "Config.h"
#include "Macros.h"
#include "Object.h"
#include "../KazMath/kazmath.h"

#define GLUT_BUILDING_LIB
#include <glew/glew.h>
#include <glut/glut.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#include <string>
#include <map>
#include <vector>
#include <unordered_map>
