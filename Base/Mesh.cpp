#include "pcHeaders.h"
#include "Mesh.h"
#include "ShaderCache.h"
#include "Texture2D.h"
#include "Camera.h"
#include "GLGlobals.h"
#include <fstream>
#include <tuple>
#include <string>
#include <cstring>
#include <sstream>

using namespace std;

Mesh::Mesh() : shader_(nullptr), texture_(nullptr), VBOID_(nullptr), usePerVertexColor_(false), autoSelectShader_(true), hasNormals_(false)
{
	uniformColor_.inUse = false;
	setShader(ShaderCache::sharedShaderCache()->getShader(SHADERNAME_POSITION));
}

Mesh::Mesh(const Mesh& ref) : shader_(ref.shader_), texture_(ref.texture_), VBOID_(ref.VBOID_), 
	usePerVertexColor_(ref.usePerVertexColor_), uniformIDs_(ref.uniformIDs_), useIndices_(ref.useIndices_),
	uniformColor_(ref.uniformColor_), indicesCount_(ref.indicesCount_), vertexAttributeIDs_(ref.vertexAttributeIDs_),
	autoSelectShader_(ref.autoSelectShader_),
	hasNormals_(ref.hasNormals_)
{
	if (shader_)
		shader_->retain();
	if(texture_)
		texture_->retain();
}

Mesh::Mesh(const VertexInfo* vertices, int vertexCount) : shader_(nullptr), texture_(nullptr), VBOID_(nullptr), usePerVertexColor_(false), autoSelectShader_(true), hasNormals_(false)
{
	uniformColor_.inUse = false;
	setShader(ShaderCache::sharedShaderCache()->getShader(SHADERNAME_POSITION));
	setData(vertices,vertexCount);	
}

Mesh::Mesh(const VertexInfo* vertices, int vertexCount, const short* indices, int indexCount) : shader_(nullptr), texture_(nullptr), VBOID_(nullptr), usePerVertexColor_(false), autoSelectShader_(true), hasNormals_(false)
{
	uniformColor_.inUse = false;
	setShader(ShaderCache::sharedShaderCache()->getShader(SHADERNAME_POSITION_TEXTURE));
	setData(vertices,vertexCount,indices,indexCount);
}

Mesh::~Mesh()
{
	if(texture_)
		texture_->release();
	if (shader_)
		shader_->release();
	if(VBOID_.use_count() == 1)
		glDeleteBuffers(2, reinterpret_cast<GLuint*>(VBOID_.get()));
}

void Mesh::render()
{
	if(!VBOID_ || !shader_ || !getVisible())
		return;
	shader_->use();
	shader_->setUniform(uniformIDs_.MMatrix,getWorldTransformationMatrix());
	shader_->setUniform(uniformIDs_.PMatrix,Camera::getActiveCamera()->getProjectionMatrix());
	shader_->setUniform(uniformIDs_.VMatrix,Camera::getActiveCamera()->getViewMatrix());
	if(uniformColor_.inUse)
		shader_->setUniform(uniformIDs_.color, uniformColor_.r, uniformColor_.g, uniformColor_.b,uniformColor_.a);
	GLGlobals::sharedGLGlobals()->setArrayBuffer(VBOID_->vertices);
	if(useIndices_)
		GLGlobals::sharedGLGlobals()->setElementArrayBuffer(VBOID_->indices);

	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.position, true);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.color, usePerVertexColor_);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.textureCoordinates, texture_ != nullptr);
	GLGlobals::sharedGLGlobals()->setVertexAttributeEnabled(vertexAttributeIDs_.normals, hasNormals_);
	glVertexAttribPointer(vertexAttributeIDs_.position, 3, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.positionOffset_);
	if (hasNormals_)
		glVertexAttribPointer(vertexAttributeIDs_.normals, 3, GL_FLOAT, true, vertexAttributeStride_, vertexAttributeOffsets_.normalOffset_);
	if (usePerVertexColor_)
		glVertexAttribPointer(vertexAttributeIDs_.color, 3, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.colorOffset_);
	if(vertexAttributeIDs_.textureCoordinates != -1)
			glVertexAttribPointer(vertexAttributeIDs_.textureCoordinates, 2, GL_FLOAT, false, vertexAttributeStride_, vertexAttributeOffsets_.texCoordOffset_);		
	if (texture_ != nullptr)
		GLGlobals::sharedGLGlobals()->bindTexture(texture_);
	if(useIndices_)
		glDrawElements(GL_TRIANGLES, indicesCount_, GL_UNSIGNED_SHORT, 0);
	else
		glDrawArrays(GL_TRIANGLES, 0,indicesCount_);
}

void Mesh::postRender()
{

}

GLuint Mesh::getArrayBufferID()
{
	if (VBOID_)
		return VBOID_->vertices;
	else
		return 0;
}

GLuint Mesh::getElementArrayBufferID()
{
	if (VBOID_)
		return VBOID_->indices;
	else
		return 0;
}

void Mesh::load(const char* path)
{
	unsigned len = strlen(path);
	for(--len;len>=0;--len)
		if(path[len] =='.')
			break;
	if(strcmp(path+len,".obj") == 0)
		objFileLoader(path);
}

void Mesh::objFileLoader(const char* path)
{
	ifstream inFile(path);
	string line;
	vector<tuple<float,float,float>> positions;
	vector<tuple<float,float>> textureCoord;
	vector<tuple<float,float,float>> normals;
	vector<vector<tuple<int,int,int>>> faces;
	map<int,vector<int>> smooth_group;
	vector<VertexInfo> vertices;
	map<tuple<int,int,int>, int> mergedVertices;
	vector<short> indices;
	int active_smooth_group = 0;
	if (path)
	{
		for(getline(inFile, line); !inFile.eof(); getline(inFile,line))
		{
			stringstream lineStream(line);
			string entryType;
			lineStream >> entryType;
			if(entryType == "v")
			{
				float x,y,z;
				lineStream>>x>>y>>z;
				positions.push_back(make_tuple(x,y,z));
			}
			if(entryType == "vn")
			{
				float x,y,z;
				lineStream>>x>>y>>z;
				normals.push_back(make_tuple(x,y,z));
			}
			if(entryType == "vt")
			{
				float u,v;
				lineStream>>u>>v;
				textureCoord.push_back(make_tuple(u,v));
			}
			if(entryType == "s")
			{
				int id = 0;
				lineStream >> id;
				active_smooth_group = id;
			}
			if(entryType == "f")
			{
				string group;
				int index[3];
				int vID = 0;
				smooth_group[active_smooth_group].push_back(faces.size());
				faces.push_back(vector<tuple<int,int,int>>());				
				for(lineStream>>group;lineStream;lineStream>>group,vID++)
				{
					if(vID == 3)
					{
						index[1] = index[2];
						vID --;
					}
					int attribs[3] = {0,0,0};
					int fID = 0;
					for(unsigned i = 0 ;i < group.size();i++)
						switch (group[i])
					{
						case '0': case '1': case '2':case '3': case '4': case '5':case '6': case '7': case '8':case '9':
							attribs[fID] = attribs[fID] * 10 + group[i] - '0';
							break;
						case '/':
							fID++;
							break;
						default : return;
					}
					auto t = make_tuple(attribs[0],attribs[1],attribs[2]);					
					faces.back().push_back(t);					
				}
			}
		}		
		for(auto &group:smooth_group)
		{
			vector<int> cnt(positions.size(),0);
			vector<float> sx(positions.size(),0),sy(positions.size(),0),sz(positions.size(),0);
			if(group.first != 0)
			{
			
				float nx,ny,nz,d;
				for(auto& i:group.second)
					for(unsigned j=0;j<faces[i].size();j++)
					{
						cnt[get<0>(faces[i][j]) - 1] ++;
#define pos(j,a) get<a>(positions[get<0>(faces[i][(j+faces[i].size()) % faces[i].size()]) - 1])
#define dif(x,y,a) (pos(y,a) - pos(x,a))
						float dx1,dx2,dy1,dy2,dz1,dz2;
						dx1 = dif(j,j+1,0);
						dx2 = dif(j,j-1,0);
						dy1 = dif(j,j+1,1);
						dy2 = dif(j,j-1,1);
						dz1 = dif(j,j+1,2);
						dz2 = dif(j,j-1,2);
						
						nx = dy1 * dz2 - dz1 * dy2;
						ny = dz1 * dx2 - dx1 * dz2;
						nz = dx1 * dy2 - dy1 * dx2;
						d = sqrt(nx * nx + ny * ny + nz * nz);
						sx[get<0>(faces[i][j]) - 1] += nx / d;
						sy[get<0>(faces[i][j]) - 1] += ny / d;
						sz[get<0>(faces[i][j]) - 1] += nz / d;
#undef pos
#undef dif
					}
			
				for(unsigned i=0;i<positions.size();i++)
				{
					sx[i] /= cnt[i];
					sy[i] /= cnt[i];
					sz[i] /= cnt[i];				
				}			
			}
			for(auto& i:group.second)
				for(unsigned j=0;j<faces[i].size();j++)
				{
					if (group.first != 0 && get<2>(faces[i][j]) == 0)
						get<2>(faces[i][j]) = -group.first;
					auto id = mergedVertices.find(faces[i][j]);
					int attribs[3],index[3];
					if(j > 2)
					{
						index[1] = index[2];
					}
					attribs[0] = get<0>(faces[i][j]);
					attribs[1] = get<1>(faces[i][j]);
					attribs[2] = get<2>(faces[i][j]);
					
					if(id == mergedVertices.end())
					{
						VertexInfo val;
						if(attribs[0] > 0)
						{
							val.x = get<0>(positions[attribs[0] - 1]);
							val.y = get<1>(positions[attribs[0] - 1]);
							val.z = get<2>(positions[attribs[0] - 1]);
						}
						if(attribs[1] > 0)
						{
							val.u = get<0>(textureCoord[attribs[1] - 1]);
							val.v = get<1>(textureCoord[attribs[1] - 1]);							
						}
						if(attribs[2] > 0)
						{
							val.nx = get<0>(normals[attribs[2] - 1]);
							val.ny = get<1>(normals[attribs[2] - 1]);
							val.nz = get<2>(normals[attribs[2] - 1]);
						}
						else
							if(attribs[2] < 0)
							{
								val.nx = sx[attribs[0] - 1];
								val.ny = sy[attribs[0] - 1];
								val.nz = sz[attribs[0] - 1];
							}
							else
							{
								val.nx = val.ny = val.nz = 0;
							}
						index [j>2?2:j] = mergedVertices[faces[i][j]] = vertices.size();
						vertices.push_back(val);
					}
					else
						index [j>2?2:j] = id->second;
					if(j >= 2)
					{
						indices.push_back(index[0]);
						indices.push_back(index[1]);
						indices.push_back(index[2]);
					}
				}
		}

		setData(vertices.data(), vertices.size(), indices.data(), indices.size());		
		setHasNormals(true);
	}
}

void Mesh::disposeData()
{
	if(VBOID_.use_count() == 1)
	{
		glDeleteBuffers(2, reinterpret_cast<GLuint*>(VBOID_.get()));
		VBOID_ = nullptr;
	}
}

void Mesh::setTexture(Texture* texture)
{
	if (texture_ == texture)
		return;
	bool usedTexture = texture_ != nullptr;
	bool nowTexture = texture != nullptr;
	if (texture_)
		texture_->release();
	texture_ = texture;
	if (texture_)
		texture_->retain();
	if (nowTexture != usedTexture && autoSelectShader_)
	{
		int shaderID = (texture_?1:0) | (usePerVertexColor_?2:0) | (uniformColor_.inUse?4:0);
		const char* shaderName;
		switch (shaderID)
		{
			case 0: shaderName = SHADERNAME_POSITION;break;
			case 1: shaderName = SHADERNAME_POSITION_TEXTURE;break;
			case 2: shaderName = SHADERNAME_POSITION_COLOR;break;
			case 3: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR;break;
			case 4: shaderName = SHADERNAME_POSITION_U_COLOR;break;
			case 5: shaderName = SHADERNAME_POSITION_TEXTURE_U_COLOR;break;
			case 6: shaderName = SHADERNAME_POSITION_COLOR_U_COLOR;break;			
			case 7: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR_U_COLOR;break;
			default: shaderName = nullptr;
		}
		setShader(ShaderCache::sharedShaderCache()->getShader(shaderName));		
	}	
}

Texture* Mesh::getTexture()
{
	return texture_;
}

void Mesh::setUsePerVertexColor(bool status)
{
	if (usePerVertexColor_ != status && autoSelectShader_)
	{
		int shaderID = (texture_?1:0) | (status?2:0) | (uniformColor_.inUse?4:0);
		const char* shaderName;
		switch (shaderID)
		{
			case 0: shaderName = SHADERNAME_POSITION;break;
			case 1: shaderName = SHADERNAME_POSITION_TEXTURE;break;
			case 2: shaderName = SHADERNAME_POSITION_COLOR;break;
			case 3: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR;break;
			case 4: shaderName = SHADERNAME_POSITION_U_COLOR;break;
			case 5: shaderName = SHADERNAME_POSITION_TEXTURE_U_COLOR;break;
			case 6: shaderName = SHADERNAME_POSITION_COLOR_U_COLOR;break;			
			case 7: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR_U_COLOR;break;
			default: shaderName = nullptr;
		}
		setShader(ShaderCache::sharedShaderCache()->getShader(shaderName));		
	}
	usePerVertexColor_ = status;
}

bool Mesh::getUsePerVertexColor()
{
	return usePerVertexColor_;
}

void Mesh::setAutoSelectShader(bool value)
{
	autoSelectShader_ = value;
}

bool Mesh::getAutoSelectShader()
{
	return autoSelectShader_;
}

void Mesh::setHasNormals(bool value)
{
	hasNormals_ = value;
}

bool Mesh::getHasNormals()
{
	return hasNormals_ ;
}

void Mesh::setUniformColor(float r, float g, float b, float a)
{
	uniformColor_.r = r;
	uniformColor_.g = g;
	uniformColor_.b = b;
	uniformColor_.a = a;
	uniformColor_.inUse = true;
	if (uniformColor_.inUse || !autoSelectShader_)
		return;
	
	int shaderID = (texture_?1:0) | (usePerVertexColor_?2:0) | 4;
	const char* shaderName;
	switch (shaderID)
	{
		case 4: shaderName = SHADERNAME_POSITION_U_COLOR;break;
		case 5: shaderName = SHADERNAME_POSITION_TEXTURE_U_COLOR;break;
		case 6: shaderName = SHADERNAME_POSITION_COLOR_U_COLOR;break;			
		case 7: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR_U_COLOR;break;
		default: shaderName = nullptr;
	}
	setShader(ShaderCache::sharedShaderCache()->getShader(shaderName));				
}

void Mesh::removeUniformColor()
{
	if (uniformColor_.inUse && autoSelectShader_)
	{		
		int shaderID = (texture_?1:0) | (usePerVertexColor_?2:0);
		const char* shaderName;
		switch (shaderID)
		{
			case 0: shaderName = SHADERNAME_POSITION;break;
			case 1: shaderName = SHADERNAME_POSITION_TEXTURE;break;
			case 2: shaderName = SHADERNAME_POSITION_COLOR;break;
			case 3: shaderName = SHADERNAME_POSITION_TEXTURE_COLOR;break;
			default: shaderName = nullptr;
		}
		setShader(ShaderCache::sharedShaderCache()->getShader(shaderName));		
	}	
	uniformColor_.inUse = false;
}

void Mesh::setShader(Shader* shader)
{
	if (!shader)
		return;
	if (shader_)
		shader_->release();
	shader_ = shader;
	shader_->retain();	
	vertexAttributeIDs_.textureCoordinates = shader_->getAttribitueID("a_TexCOORD");
	vertexAttributeIDs_.position= shader_->getAttribitueID("a_Position");
	vertexAttributeIDs_.normals = shader_->getAttribitueID("a_Normal");
	vertexAttributeIDs_.color = shader_->getAttribitueID("a_Color");
	uniformIDs_.PMatrix = shader_->getUniformID("u_Pmat");
	uniformIDs_.VMatrix = shader_->getUniformID("u_Vmat");
	uniformIDs_.MMatrix = shader_->getUniformID("u_Mmat");
	uniformIDs_.color = shader_->getUniformID("u_Color");	
}

Shader* Mesh::getShader()
{
	return shader_;
}