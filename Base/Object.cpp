#include "pcHeaders.h"
#include "Object.h"
#include "GarbageCollector.h"

Object::Object() : retainCount_ (0)
{
	GarbageCollector::sharedGarbageCollector()->pushObject(this);
}

Object::~Object()
{
	
}

void Object::retain()
{
	retainCount_ ++;
}

void Object::release()
{
	if(--retainCount_ == 0)
		delete this;
}

