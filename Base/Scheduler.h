#include <vector>
#include <set>

class UpdateableProtocol;

class Scheduler
{
public:
	Scheduler();
	~Scheduler();
	static Scheduler* sharedScheduler();
	static void purgeSceduler();
	void step(float dt);
private:
	friend class UpdateableProtocol;
	static Scheduler* sharedScheduler_;
	std::vector<UpdateableProtocol*> updateList_;
	std::vector<unsigned> removeList_;
	std::vector<UpdateableProtocol*> addList_;
	bool listLocked_;
	void addTarget(UpdateableProtocol* target);
	void removeTarget(UpdateableProtocol* target);
};