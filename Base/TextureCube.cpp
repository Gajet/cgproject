#include "pcHeaders.h"
#include "TextureCube.h"
#include <IL/il.h>
#include <gl/GL.h>
#include <iostream>
#include "GLGlobals.h"

TextureCube::TextureCube()
{
	
	GLGlobals::sharedGLGlobals()->bindTexture(this);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
	minifyFilter_ = magnifyFilter_ = GL_LINEAR;
	wrapModeS_ = wrapModeT_ = wrapModeR_ = GL_CLAMP_TO_EDGE;
}

TextureCube::~TextureCube()
{	
}

void TextureCube::setWrapMode(GLenum wrapModeS, GLenum wrapModeT, GLenum wrapModeR)
{
	if(wrapModeS != wrapModeS_ || wrapModeT != wrapModeT_ || wrapModeR != wrapModeR_)
		GLGlobals::sharedGLGlobals()->bindTexture(this);
	if(wrapModeS != wrapModeS_)
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, wrapModeS);
	if(wrapModeT != wrapModeT_)
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, wrapModeT);
	if(wrapModeR != wrapModeR_)
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, wrapModeR);
	wrapModeS_ = wrapModeS;
	wrapModeT_ = wrapModeT;
	wrapModeR_ = wrapModeR;
}

void TextureCube::setScaleFilter(GLenum minifyFilter, GLenum magnifyFilter)
{
	if(minifyFilter != minifyFilter_ || magnifyFilter != magnifyFilter_)
		GLGlobals::sharedGLGlobals()->bindTexture(this);
	if(minifyFilter != minifyFilter_)
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, minifyFilter);
	if(magnifyFilter != magnifyFilter_)
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, magnifyFilter);
	minifyFilter_ = minifyFilter;
	magnifyFilter_ = magnifyFilter_;
}
GLenum TextureCube::getType()
{
	return GL_TEXTURE_CUBE_MAP;
}
