#include "pcHeaders.h"
#include "MeshFactory.h"
#include "Mesh.h"
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

MeshFactory::MeshFactory() : Object()
{

}

Mesh* MeshFactory::createSphere(unsigned vSlice, unsigned hSlice)
{
	if(vSlice == 0 || hSlice == 0)
		return nullptr;
	return assignSphere(new Mesh, vSlice,hSlice);
}

Mesh* MeshFactory::assignSphere(Mesh* target, unsigned vSlice, unsigned hSlice)
{
	if(vSlice == 0 || hSlice == 0)
		return target;
	Mesh::VertexInfo* vertices = new Mesh::VertexInfo[(vSlice * 2) * (hSlice - 1) * 2 * 3 + (vSlice * 2) * 2 * 3];
	int vid;
	vertices[0].x = 0;
	vertices[0].y = 0;
	vertices[0].z = 0;			
	vid = 0;
	for(float i=1;i<hSlice;i++)
		for(float j=0;j<vSlice * 2;j++)
		{
			vertices[vid].y = sin((i + 0.5f) * M_PI / hSlice);
			vertices[vid].x = vertices[vid].y * cos(j * M_PI / vSlice);
			vertices[vid].z = vertices[vid].y * sin(j * M_PI / vSlice);
			vertices[vid].y = cos((i + 0.5f) * M_PI / hSlice);
			vertices[vid].u = j / vSlice / 2.0f;
			vertices[vid].v = (i+0.5f) / hSlice;
			vid ++;	
			vertices[vid].y = sin((i - 0.5f) * M_PI / hSlice);
			vertices[vid].x = vertices[vid].y * cos((j+1) * M_PI / vSlice);
			vertices[vid].z = vertices[vid].y * sin((j+1) * M_PI / vSlice);
			vertices[vid].y = cos((i - 0.5f) * M_PI / hSlice);
			vertices[vid].u = (j+1) / vSlice / 2.0f;
			vertices[vid].v = (i-0.5f) / hSlice;
			vid ++;	
			vertices[vid].y = sin((i + 0.5f) * M_PI / hSlice);
			vertices[vid].x = vertices[vid].y * cos((j + 1) * M_PI / vSlice);
			vertices[vid].z = vertices[vid].y * sin((j + 1) * M_PI / vSlice);
			vertices[vid].y = cos((i + 0.5f) * M_PI / hSlice);
			vertices[vid].u = (j+1) / vSlice / 2.0f;
			vertices[vid].v = (i+0.5f) / hSlice;
			vid ++;	

			vertices[vid].y = sin((i + 0.5f) * M_PI / hSlice);
			vertices[vid].x = vertices[vid].y * cos((j+0) * M_PI / vSlice);
			vertices[vid].z = vertices[vid].y * sin((j+0) * M_PI / vSlice);
			vertices[vid].y = cos((i + 0.5f) * M_PI / hSlice);
			vertices[vid].u = j / vSlice / 2.0f;
			vertices[vid].v = (i+0.5f) / hSlice;
			vid ++;	
			vertices[vid].y = sin((i - 0.5f) * M_PI / hSlice);
			vertices[vid].x = vertices[vid].y * cos(j * M_PI / vSlice);
			vertices[vid].z = vertices[vid].y * sin(j * M_PI / vSlice);
			vertices[vid].y = cos((i - 0.5f) * M_PI / hSlice);
			vertices[vid].u = j / vSlice / 2;
			vertices[vid].v = (i-0.5) / hSlice;
			vid ++;	
			vertices[vid].y = sin((i - 0.5f) * M_PI / hSlice);
			vertices[vid].x = vertices[vid].y * cos((j + 1) * M_PI / vSlice);
			vertices[vid].z = vertices[vid].y * sin((j + 1) * M_PI / vSlice);
			vertices[vid].y = cos((i - 0.5f) * M_PI / hSlice);
			vertices[vid].u = (j+1) / vSlice / 2;
			vertices[vid].v = (i-0.5) / hSlice;
			vid ++;	
		}
		
		for(unsigned j=0;j<vSlice * 2;j++)
	{		
		vertices[vid].y = sin((hSlice - 0.5f) * M_PI / hSlice);
		vertices[vid].x = vertices[vid].y * cos(j * M_PI / vSlice);
		vertices[vid].z = vertices[vid].y * sin(j * M_PI / vSlice);
		vertices[vid].y = cos((hSlice - 0.5f) * M_PI / hSlice);
		vertices[vid].u = j / vSlice / 2.0f;
		vertices[vid].v = (hSlice-0.5f) / hSlice;
		vid ++;			
		vertices[vid].y = sin((hSlice - 0.5f) * M_PI / hSlice);
		vertices[vid].x = vertices[vid].y * cos((j + 1) * M_PI / vSlice);
		vertices[vid].z = vertices[vid].y * sin((j + 1) * M_PI / vSlice);
		vertices[vid].y = cos((hSlice - 0.5f) * M_PI / hSlice);
		vertices[vid].u = (j+1) / vSlice / 2.0f;
		vertices[vid].v = (hSlice-0.5f) / hSlice;
		vid ++;	
		vertices[vid].x = 0;
		vertices[vid].z = 0;
		vertices[vid].y = -1;
		vertices[vid].u = 0.5f;
		vertices[vid].v = 1;
		vid ++;	
		
	}

	for(unsigned j=0;j<vSlice * 2;j++)
	{		
		vertices[vid].y = sin((0.5f) * M_PI / hSlice);
		vertices[vid].x = vertices[vid].y * cos(j * M_PI / vSlice);
		vertices[vid].z = vertices[vid].y * sin(j * M_PI / vSlice);
		vertices[vid].y = cos((0.5f) * M_PI / hSlice);
		vertices[vid].u = j / vSlice / 2.0f;
		vertices[vid].v = (0.5f) / hSlice;
		vid ++;			
		vertices[vid].x = 0;
		vertices[vid].z = 0;
		vertices[vid].y = 1;
		vertices[vid].u = 0.5f;
		vertices[vid].v = 0;
		vid ++;	
		vertices[vid].y = sin((0.5f) * M_PI / hSlice);
		vertices[vid].x = vertices[vid].y * cos((j + 1) * M_PI / vSlice);
		vertices[vid].z = vertices[vid].y * sin((j + 1) * M_PI / vSlice);
		vertices[vid].y = cos((0.5f) * M_PI / hSlice);
		vertices[vid].u = (j+1) / vSlice / 2.0f;
		vertices[vid].v = (0.5f) / hSlice;
		vid ++;	
				
	}
	
	target->setData(vertices, vid);
	delete []vertices;
	return target;
}