#include "pcHeaders.h"
#include "PositionProtocol.h"
#include "Camera.h"

PositionProtocol::PositionProtocol() : transformDirty_(true), parent_(nullptr)
{
	position_.x = 0;
	position_.y = 0;
	position_.z = 0;
	rotation_.x = 0;
	rotation_.y = 0;
	rotation_.z = 0;
	scale_.x = 1;
	scale_.y = 1;
	scale_.z = 1;
}

PositionProtocol::~PositionProtocol()
{
	parent_ = nullptr;
}

void PositionProtocol::recalculateWorldTransformationMatrix(bool recurse)
{
	if (parent_)
	{
		if (recurse)
			parent_->recalculateWorldTransformationMatrix(true);
		kmMat4Multiply(&worldTransformationMatrix_, &parent_->worldTransformationMatrix_, &getTransformationMatrix());
	}
	else
	{
		//if (Camera::getActiveCamera())
			//kmMat4Multiply(&worldTransformationMatrix_, &Camera::getActiveCamera()->getProjectionViewMatrix(), &getTransformationMatrix());
		//else
		kmMat4Assign(&worldTransformationMatrix_, &getTransformationMatrix());
	}
}

const kmMat4& PositionProtocol::getTransformationMatrix()
{
	if (transformDirty_)
	{
		kmMat4 temp;
		kmMat4Identity(&transformMatrix_);
		kmMat4Translation(&temp, position_.x, position_.y, position_.z);
		kmMat4Multiply(&transformMatrix_, &transformMatrix_, &temp);
		kmMat4RotationX(&temp, rotation_.x);
		kmMat4Multiply(&transformMatrix_, &transformMatrix_, &temp);
		kmMat4RotationY(&temp, rotation_.y);
		kmMat4Multiply(&transformMatrix_, &transformMatrix_, &temp);
		kmMat4RotationZ(&temp, rotation_.z);
		kmMat4Multiply(&transformMatrix_, &transformMatrix_, &temp);
		kmMat4Scaling(&temp, scale_.x, scale_.y, scale_.z);
		kmMat4Multiply(&transformMatrix_, &transformMatrix_, &temp);
	}
	transformDirty_ = false;
	return transformMatrix_;
}

const kmMat4& PositionProtocol::getWorldTransformationMatrix()
{
	return worldTransformationMatrix_;
}

void PositionProtocol::setPosition(kmVec3 position)
{
	setPosition(position.x, position.y, position.z);
}

kmVec3 PositionProtocol::getPosition()
{
	return position_;
}

void PositionProtocol::setRotation(kmVec3 Rotation)
{
	setRotation(Rotation.x, Rotation.y, Rotation.z);
}

kmVec3 PositionProtocol::getRotation()
{
	return rotation_;
}

void PositionProtocol::setScale(kmVec3 Scale)
{
	setScale(Scale.x, Scale.y, Scale.y);
}

kmVec3 PositionProtocol::getScale()
{
	return scale_;
}

void PositionProtocol::setPosition(PRECISION x, PRECISION y, PRECISION z)
{
	transformDirty_ = true;
	position_.x = x;
	position_.y = y;
	position_.z = z;
}

void PositionProtocol::setRotation(PRECISION x, PRECISION y, PRECISION z)
{
	transformDirty_ = true;
	rotation_.x = x;
	rotation_.y = y;
	rotation_.z = z;
}

void PositionProtocol::setScale(PRECISION x, PRECISION y, PRECISION z)
{
	transformDirty_ = true;
	scale_.x = x;
	scale_.y = y;
	scale_.z = z;
}

PositionProtocol* PositionProtocol::getParent()
{
	return parent_;
}

