#include "pcHeaders.h"
#include "DefaultShaders.h"

#define STRINGIFY(X) #X

const char* ShaderPositionition_v = STRINGIFY(
	#version 120									\n
													\n
	uniform mat4 u_Mmat;							\n
	uniform mat4 u_Vmat;							\n
	uniform mat4 u_Pmat;							\n
	attribute vec4 a_Position;						\n	
													\n
	void main()										\n
	{												\n
		gl_Position = u_Pmat * u_Vmat * u_Mmat * a_Position;\n

	}
	);

const char* ShaderPositionition_f = STRINGIFY(
	#version 120									\n	
													\n
	void main()										\n
	{												\n
		gl_FragColor = vec4(1,1,1,1);				\n
	}
	);