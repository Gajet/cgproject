#include "pcHeaders.h"
#include "TextureCache.h"

using namespace std;

TextureCache* TextureCache::sharedTextureCache_ = nullptr;

TextureCache::TextureCache()
{
}

TextureCache::~TextureCache()
{
	for(auto&i : cachedTextures_)
	{
		i.second->release();
	}
}

TextureCache* TextureCache::sharedTextureCache()
{
	if(sharedTextureCache_ == nullptr)
		sharedTextureCache_ = new TextureCache;
	return sharedTextureCache_;
}

Texture2D* TextureCache::addTexture(const char* path)
{
	auto i = cachedTextures_.find(path);
	if (i == cachedTextures_.end())
	{
		Texture2D* res = new Texture2D(path);
		res->retain();
		cachedTextures_[path] = res;
		return res;
	}
	return i->second;
}

void TextureCache::removeTexture(const char* path)
{
	auto i = cachedTextures_.find(path);
	if (i == cachedTextures_.end())
		return;
	i->second->release();
	cachedTextures_.erase(i);
}

void TextureCache::removeTexture(Texture2D* texture)
{
	for(auto i = cachedTextures_.begin();i != cachedTextures_.end();i++)
		if(i->second==texture)
		{
			texture->release();
			cachedTextures_.erase(i);
			return;
		}
}

